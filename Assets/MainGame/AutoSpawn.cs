﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpawn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", 0f, 5f);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

	//Spawn
	void Spawn(){
		int rand = Random.Range(1, 3);
		if (rand == 1)
		{
			float x, y, z;
			x = Random.Range(0, 14);
			y = Random.Range(2, 14);
			z = Random.Range(2, 14);
			Instantiate(GameObject.Find("targetFab1"), new Vector3(x, y, z), Quaternion.identity);
		} else if (rand == 2)
		{
			float x, y, z;
			x = Random.Range(2, 14);
			y = Random.Range(2, 14);
			z = Random.Range(2, 14);
			Instantiate(GameObject.Find("targetFab2"), new Vector3(x, y, z), Quaternion.identity);

		}
	}
}
