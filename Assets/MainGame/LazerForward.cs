﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerForward : MonoBehaviour {

	public float Speed = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * Speed * Time.deltaTime;
	}
}
