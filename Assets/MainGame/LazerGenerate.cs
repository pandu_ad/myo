﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Thalmic.Myo;
using System;

public class LazerGenerate : MonoBehaviour {
	public GameObject Myo;
    public DateTime lastShot;
	// Use this for initialization
	void Start () {
        lastShot = System.DateTime.Now;
    }
	
	// Update is called once per frame
	void Update () {
		ThalmicMyo thalmicMyo = Myo.GetComponent<ThalmicMyo> ();
		if ((thalmicMyo.pose == Pose.WaveOut || thalmicMyo.pose == Pose.FingersSpread) && ((System.DateTime.Now.Subtract(lastShot)).TotalMilliseconds >= 1000)) {
			GameObject lazer = Instantiate (Resources.Load ("Lazer")) as GameObject;
			lazer.transform.position = gameObject.transform.position;
			lazer.transform.forward = Camera.main.transform.forward;
            lastShot = DateTime.Now;
		} 
    }
}
