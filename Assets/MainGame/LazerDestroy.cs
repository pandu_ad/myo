﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LazerDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject mainWeapon = GameObject.Find ("Weapon") as GameObject;

		if ((gameObject.transform.position - mainWeapon.transform.position).magnitude > 50.0f) {
			Destroy (gameObject);
		}
	}
}
