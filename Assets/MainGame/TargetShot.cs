﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetShot : MonoBehaviour
{
    public GameObject Explotion;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision col)
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        Explotion.SetActive(true);

        Destroy(col.gameObject);
        gameObject.SetActive(false);
        Invoke("DestroyObject", 10);

        GameObject score = GameObject.Find("Score");
        ScoreManager scoreManager = score.GetComponent("ScoreManager") as ScoreManager;
        scoreManager.Score++;
    }

    void DestroyObject()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }
}
